﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;

using ZedGraph;
using MotusNS;

namespace MotusMonitor
{
    public partial class Form1 : Form
    {
        int _inuse = -1;

        /// <summary>
        /// Costruttore di Supporto
        /// </summary>
        private void InitializeExEvent()
        {
            _emuHandler.OnConnect += new OnConnectHandler(HandlerClientOnConnect);
            _emuHandler.OnUpdate  += new OnUpdateHandler(HandlerOnUpdate);
            _emuHandler.OnRemove  += new OnRemoveHandler(HandlerClientOnRemoved);
            _emuHandler.OnReceive += new OnReceiveHandler(HandlerClientOnReceived);
        }

        /// <summary>
        /// Gestione Update
        /// </summary>
        public void HandlerOnUpdate(object o)
        {
            toolStripStatusLabel7.Text = DateTime.Now.ToLocalTime().ToString();
        }

        /// <summary>
        /// Gestione client connesso
        /// </summary>
        public void HandlerClientOnConnect(object o, ClientConnectEventArgs e)
        {
            lock (this._messages)
            {
                this._messages.Add("0|Client '" + e.Client.ID + "' connesso al server.");
                this.toolStripStatusLabel3.Text = this._messages.Count.ToString();
            }

            lock (this.listView1)
            {
                this.listView1.Items.Clear();
                for (int i = 0; i < this._emuHandler.GetClientAvailable(); i++)
                {
                    List<String> tmp = new List<string>();
                    tmp.Add(this._emuHandler.GetClient(i).Index.ToString());
                    tmp.Add(this._emuHandler.GetClient(i).ID);
                    tmp.Add(this._emuHandler.GetClient(i).Frequency.ToString());

                    if (i == 0 && _inuse == -1)
                    {
                        _inuse = this._emuHandler.GetClient(i).Index;
                        label1.Text = "CLIENT ID:  " + this._emuHandler.GetClient(i).ID;
                        label1.Text += ", Hz:  " + this._emuHandler.GetClient(i).Frequency.ToString();
                        this._freqInUse = this._emuHandler.GetClient(i).Frequency;
                        zedGraphControl1.Invalidate();
                    }

                    this.listView1.Items.Add(new ListViewItem(tmp.ToArray()));
                }
            }
        }

        /// <summary>
        /// Gestione client rimosso
        /// </summary>
        public void HandlerClientOnRemoved(object o, ClientRemovedEventArgs e)
        {
            lock (this._messages)
            {
                this._messages.Add("0|Client '" + e.Client.ID + "' disconnesso dal server.");
                this.toolStripStatusLabel3.Text = this._messages.Count.ToString();
            }

            lock (this.listView1)
            {
                this.listView1.Items.Clear();
                for (int i = 0; i < this._emuHandler.GetClientAvailable(); i++)
                {
                    List<String> tmp = new List<string>();
                    tmp.Add(this._emuHandler.GetClient(i).Index.ToString());
                    tmp.Add(this._emuHandler.GetClient(i).ID);
                    tmp.Add(this._emuHandler.GetClient(i).Frequency.ToString());

                    this.listView1.Items.Add(new ListViewItem(tmp.ToArray()));
                }

                if (this._emuHandler.GetClientAvailable() <= 0)
                {
                    _inuse = -1;
                    label1.Text = "<<<  Selezionare un Client dal menù a lato...";
                }
            }
        }

        /// <summary>
        /// Gestione ricezione dati dal client
        /// </summary>
        public void HandlerClientOnReceived(object o, ClientReceivedEventArgs e)
        {
            if (e.Client.Index != _inuse)
                return;

            DataHeader _data = e.Client.GetDataFromStorage();

            // MOD ACC
            float modacc = (float) Math.Sqrt((_data.Accelerometer.X * _data.Accelerometer.X) +
                                             (_data.Accelerometer.Y * _data.Accelerometer.Y) +
                                             (_data.Accelerometer.Z * _data.Accelerometer.Z));

            // MOD GYR
            float modgyr = (float) Math.Sqrt((_data.Gyroscope.X * _data.Gyroscope.X) +
                                             (_data.Gyroscope.Y * _data.Gyroscope.Y) +
                                             (_data.Gyroscope.Z * _data.Gyroscope.Z));

            // ARCTAN MAG
            float arctanmag = 0f;            
            if (_data.Magnetometer.X == 0f && _data.Magnetometer.Z > 0f)
            {
                arctanmag = (float) (Math.PI) / 2;
            }
            
            if (_data.Magnetometer.X == 0f && _data.Magnetometer.Z < 0f)
            {
                arctanmag = (float) (3 * (Math.PI)) / 2;
            }
            
            if ((_data.Magnetometer.X > 0f && _data.Magnetometer.Z > 0f))
            {
                arctanmag = (float) Math.Atan(_data.Magnetometer.Z / _data.Magnetometer.X);
            }

            if (_data.Magnetometer.X < 0f && _data.Magnetometer.Z < 0f)
            {
                arctanmag = (float) ((2 * Math.PI) - Math.Atan(_data.Magnetometer.Z / _data.Magnetometer.X));
            }

            if ((_data.Magnetometer.X > 0f && _data.Magnetometer.Z < 0f) ||
                (_data.Magnetometer.X < 0f && _data.Magnetometer.Z > 0f))
            {
                arctanmag = (float) (Math.Atan(_data.Magnetometer.Z / _data.Magnetometer.X) + Math.PI);
            }

            this._graphHandler[0].CurveList.Clear();

            this.AddGraphValue(0, 0, _data.Sequence, modacc);
            this.AddGraphValue(1, 0, _data.Sequence, modgyr);
            this.AddGraphValue(2, 0, _data.Sequence, (float)Math.Sin(arctanmag));

            this._graphHandler[0].AddCurve("Mod. Accelerometer", this._lineChart1[0], Color.Red, ZedGraph.SymbolType.None);
            this._graphHandler[0].AddCurve("Mod. Gyroscope", this._lineChart1[1], Color.Green, ZedGraph.SymbolType.None);
            this._graphHandler[0].AddCurve("Atan. Magnetometer", this._lineChart1[2], Color.LightBlue, ZedGraph.SymbolType.None);

            this._graphHandler[0].AxisChange();
            zedGraphControl1.Refresh();
        }
    }
}
