﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;

using MotusNS;
using ZedGraph;

namespace MotusMonitor
{
    public partial class Form1 : Form
    {
        List<String>    _messages;
        MoTUSNetwork    _emuHandler;
        PointPairList[] _lineChart1;
        GraphPane[]     _graphHandler;
        int             _freqInUse;

        public Form1()
        {
            InitializeComponent();
            this._graphHandler  = new GraphPane[2];
            this._messages      = new List<string>();
            this._emuHandler    = new MoTUSNetwork(8000);
            this._lineChart1    = new PointPairList[3];

            for (int i = 0; i < this._lineChart1.Length; i++)
                this._lineChart1[i] = new PointPairList();

            this._freqInUse = 100;

            this._graphHandler[0] = zedGraphControl1.GraphPane;
            CreateChart(zedGraphControl1, "Accelerometer, Magnetometer & Gyroscope");

            InitializeExEvent();
        }

        public void CreateChart(ZedGraphControl zgc, String name)
        {
            GraphPane myPane = zgc.GraphPane;

            // Set the titles and axis labels
            myPane.Title.Text = name;
            myPane.XAxis.Title.Text = "Sequence";
            myPane.YAxis.IsVisible = false;

            // Make the Y axis scale red
            myPane.YAxis.Scale.FontSpec.FontColor = Color.Red;
            myPane.YAxis.Title.FontSpec.FontColor = Color.Red;

            // turn off the opposite tics so the Y tics don't show up on the Y2 axis
            myPane.YAxis.MajorTic.IsOpposite = false;
            myPane.YAxis.MinorTic.IsOpposite = false;
            
            // Don't display the Y zero line
            myPane.YAxis.MajorGrid.IsZeroLine = false;
            
            // Align the Y axis labels so they are flush to the axis
            myPane.YAxis.Scale.Align = AlignP.Inside;
            myPane.YAxis.Scale.Max = 20;
            myPane.YAxis.Scale.Min = -5;
            myPane.XAxis.Scale.Min = 0;
            myPane.XAxis.Scale.Max = 100;
            myPane.YAxis.Cross = 0;
            myPane.Chart.Fill = new Fill(Color.Black);

            // Calculate the Axis Scale Ranges
            zgc.AxisChange();
        }

        /// <summary>
        /// Permette di aggiungere un punto al Render Graph
        /// </summary>
        public bool AddGraphValue(int i, int n, float x, float y)
        {
            if (i >= 0 && i < this._lineChart1.Length)
            {
                if (this._lineChart1[i].Count > this._freqInUse)
                {
                    this._lineChart1[i].RemoveAt(0);
                    this._lineChart1[i].Add(x, y);
                }
                else this._lineChart1[i].Add(x, y);

                this._graphHandler[n].XAxis.Scale.Min = (x - this._freqInUse < 0) ? this._freqInUse : x - this._freqInUse;
                this._graphHandler[n].XAxis.Scale.Max = x;
                
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Esecuzione procedure pre-exit
        /// </summary>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        /// <summary>
        /// Abilita/Disabilita Fullscreen
        /// </summary>
        private void abilitaDisabilitaFullscreenToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            bool wstate = (WindowState == FormWindowState.Maximized);
            bool bstate = (FormBorderStyle == System.Windows.Forms.FormBorderStyle.None);

            if (wstate && bstate)
            {
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                WindowState = FormWindowState.Normal;
            }
            else
            {
                WindowState = FormWindowState.Normal;
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
            }
        }

        /// <summary>
        /// Chiude applicazione e sotto-processi
        /// </summary>
        private void esciToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Permette di gestire l'evento post selezione riga
        /// </summary>
        private void OnItemActivate(object sender, EventArgs e)
        {
            _inuse = Int32.Parse(((ListView)sender).SelectedItems[0].Text);
            label1.Text = "CLIENT ID:  " + ((ListView)sender).SelectedItems[0].SubItems[1].Text;
            label1.Text += ", Hz:  " + ((ListView)sender).SelectedItems[0].SubItems[2].Text;
            this._freqInUse = Int32.Parse(((ListView)sender).SelectedItems[0].SubItems[2].Text);
            this._graphHandler[0].CurveList.Clear();
            zedGraphControl1.Invalidate();
        }

        /// <summary>
        /// Massimizza lo stato dell'applicazione
        /// </summary>
        private void massimizzaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool wstate = (WindowState == FormWindowState.Maximized);
            bool bstate = (FormBorderStyle == System.Windows.Forms.FormBorderStyle.None);

            if (wstate && bstate)
            {
                WindowState = FormWindowState.Normal;
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                WindowState = FormWindowState.Maximized;
            }
            else WindowState = FormWindowState.Maximized;
        }

        /// <summary>
        /// Minimizza lo stato dell'applicazione
        /// </summary>
        private void minimizzaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool wstate = (WindowState == FormWindowState.Maximized);
            bool bstate = (FormBorderStyle == System.Windows.Forms.FormBorderStyle.None);

            if (wstate && bstate)
            {
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                WindowState = FormWindowState.Minimized;
            }
            else WindowState = FormWindowState.Minimized;
        }

        /// <summary>
        /// Normalizza lo stato dell'applicazione
        /// </summary>
        private void normalizzaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool wstate = (WindowState == FormWindowState.Maximized);
            bool bstate = (FormBorderStyle == System.Windows.Forms.FormBorderStyle.None);

            if (wstate && bstate)
            {
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                WindowState = FormWindowState.Normal;
            }
            else WindowState = FormWindowState.Normal;
        }

        /// <summary>
        /// Visualizza tutti i messaggi di avviso in una Form
        /// </summary>
        private void visualizzaAvvisiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lock (this._messages)
            {
                if (this._messages.Count > 0)
                {
                    SummaryForm sp = new SummaryForm(this._messages);
                    sp.Show();
                }
                else MessageBox.Show("Nessun avviso disponibile!", "Summary Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Cancella tutti i messaggi di avviso
        /// </summary>
        private void cancellaAvvisiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lock (this._messages)
            {
                this._messages.Clear();
                this.toolStripStatusLabel3.Text = this._messages.Count.ToString();
            }
        }
    }
}
