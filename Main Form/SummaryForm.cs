﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MotusMonitor
{
    public partial class SummaryForm : Form
    {
        public SummaryForm(List<String> _data)
        {
            InitializeComponent();
            
            foreach (String s in _data)
            {
                ListViewItem it = new ListViewItem();
                it.Text = "   " + s.Split('|')[1];
                it.ImageIndex = Int32.Parse(s.Split('|')[0]);

                this.listView1.Items.Add(it);
            }
        }
    }
}
