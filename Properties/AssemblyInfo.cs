﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MoTUS Monitor")]
[assembly: AssemblyDescription("Applicativo per la gestione remota di Sensori MoTUS")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vanzo Luca Samuele, Bicocca (MI)")]
[assembly: AssemblyProduct("Motus Monitor")]
[assembly: AssemblyCopyright("Copyright ©  2013, Vanzo Luca Samuele")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e97cc028-7100-42ee-a4a0-98cb44b641c3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.1.0")]
[assembly: AssemblyFileVersion("1.0.1.0")]
